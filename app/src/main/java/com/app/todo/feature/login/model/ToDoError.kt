package com.app.todo.feature.login.model

data class ToDoError(
    val error: String
)