package com.app.todo.feature.login.model

enum class LoginStatus {
    UNKNOWN,
    VALIDATION_ERROR,
    VALIDATION_SUCCESS,
    API_ERROR,
    COMPLETED
}
