package com.app.todo.feature.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.todo.feature.login.model.LoginResponse
import com.app.todo.feature.login.model.LoginStatus
import com.app.todo.feature.login.repository.LoginRepository
import com.app.todo.feature.login.repository.request.LoginRequest
import com.app.todo.util.isEmailValid
import com.haroldadmin.cnradapter.NetworkResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginViewModel(
    private val repository: LoginRepository,
    private val backgroundDispatcher: CoroutineDispatcher = Dispatchers.Default,
) : ViewModel() {

    private var _loginStatus = MutableLiveData<LoginStatus>()
    val loginStatus: LiveData<LoginStatus>
        get() = _loginStatus
    var loginResponse: LoginResponse? = null

    var errorMessage: String? = null
        private set


    fun loginWithCredentials(email: String?, password: String?, onCompleted: () -> Unit) {
        val isValidCredentials = isEmailValid(email) && !password.isNullOrEmpty()
        _loginStatus.value =
            if (!isValidCredentials) LoginStatus.VALIDATION_ERROR else LoginStatus.VALIDATION_SUCCESS
        if (isValidCredentials) {
            viewModelScope.launch(backgroundDispatcher) {
                if (email != null && password != null) {
                    val networkResponse =
                        repository.login(LoginRequest(email = email, password = password))
                    when (networkResponse) {
                        is NetworkResponse.Success -> {
                            loginResponse = networkResponse.body
                            _loginStatus.postValue(LoginStatus.COMPLETED)
                            onCompleted()
                        }
                        is NetworkResponse.NetworkError -> {
                            errorMessage = networkResponse.error.localizedMessage
                            _loginStatus.postValue(LoginStatus.API_ERROR)
                        }
                        is NetworkResponse.ServerError -> {
                            errorMessage = networkResponse.body?.error
                            _loginStatus.postValue(LoginStatus.API_ERROR)
                        }
                        else -> {
                        }
                    }

                }

            }
        }

    }


    fun getAPIErrorMessage(): String? = errorMessage
}