package com.app.todo.feature.login.repository

import com.app.todo.feature.login.model.LoginResponse
import com.app.todo.feature.login.model.ToDoError
import com.app.todo.feature.login.repository.request.LoginRequest
import com.haroldadmin.cnradapter.NetworkResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginService {

    @POST("login")
    suspend fun fetchLoginDetails(@Body loginRequest: LoginRequest): NetworkResponse<LoginResponse, ToDoError>
}
