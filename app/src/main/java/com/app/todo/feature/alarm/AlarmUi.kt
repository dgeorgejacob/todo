package com.app.todo.feature.alarm

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.app.todo.R
import com.app.todo.ui.elements.*

@Composable
fun AlarmUI(alarmViewModel: AlarmViewModel? = null) {
    val alarmValidationStatus = alarmViewModel?.status?.observeAsState()?.value
    val currentAlarm = alarmViewModel?.currentAlarm?.observeAsState()?.value

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        FormTextField(
            placeHolderText = stringResource(id = R.string.title),
            defaultText = currentAlarm?.title,
            keyboardType = KeyboardType.Text,
        ) {
            alarmViewModel?.setTitle(it)
        }

        FormTextField(
            placeHolderText = stringResource(id = R.string.description),
            defaultText = currentAlarm?.description,
            keyboardType = KeyboardType.Text
        ) {
            alarmViewModel?.setDescription(it)
        }

        CalendarInput(
            placeHolderText = R.string.date,
            icon = R.drawable.ic_calendar,
            defaultText = currentAlarm?.date
        ) {
            alarmViewModel?.setDate(it)
        }
        TimeInput(
            placeHolderText = R.string.time,
            icon = R.drawable.ic_time_24,
            defaultTime = Time(currentAlarm?.hour ?: 0, currentAlarm?.min ?: 0)
        ) {
            alarmViewModel?.setTime(it)
        }
        RepeatTypeRadioGroup {
            alarmViewModel?.setRepeatType(it)
        }
        Button(
            onClick = { alarmViewModel?.saveAlarm() },
            modifier = Modifier.padding(top = 10.dp)
        ) {
            Text(
                text = if (currentAlarm == null) stringResource(id = R.string.create) else stringResource(
                    id = R.string.update
                )
            )
        }

        if (currentAlarm != null) {
            Button(
                onClick = { alarmViewModel?.saveAlarm() },
                modifier = Modifier.padding(top = 10.dp)
            ) {
                Text(text = stringResource(id = R.string.delete))
            }
        }

        if (alarmValidationStatus != FormValidationStatus.SUCCESS) {
            Text(
                text = alarmValidationStatus?.errorMessage ?: "", color = Color.Red,
                modifier = Modifier.padding(top = 20.dp)
            )
        }
    }

}

@Preview(showSystemUi = true)
@Composable
fun AlarmFormPreview() {
    AlarmUI()
}