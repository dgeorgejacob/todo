package com.app.todo.feature.alarm.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Alarm::class],version = 1)
abstract class AppDatabase: RoomDatabase() {
    abstract fun alarmDao():AlarmDao
}

fun createDataBase(context: Context) =  Room.databaseBuilder(
    context,
    AppDatabase::class.java, "alarm-database"
).build()