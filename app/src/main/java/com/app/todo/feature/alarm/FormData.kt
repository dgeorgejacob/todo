package com.app.todo.feature.alarm

import com.app.todo.ui.elements.Time

data class FormData(
    var title: String? = null,
    var description:String? = null,
    var date:String? = null,
    var time: Time? = null,
    var repeat:String? = null,

)
