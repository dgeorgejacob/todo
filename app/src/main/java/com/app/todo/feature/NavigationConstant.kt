package com.app.todo.feature

const val NAVIGATION_LOGIN = "login"
const val NAVIGATION_TODO_LIST = "todo_list"
const val NAVIGATION_FORM = "todo_form/{alarm_id}"