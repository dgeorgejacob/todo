package com.app.todo.feature.alarm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.todo.feature.alarm.room.Alarm
import com.app.todo.feature.alarm.room.AlarmDao
import com.app.todo.ui.elements.Time
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AlarmViewModel(
    private val alarmDao: AlarmDao,
    private val argument: String? = null,
    private val backgroundDispatcher: CoroutineDispatcher = Dispatchers.Default
) : ViewModel() {

    private val formData = FormData(repeat = "Daily")

    private val _status = MutableLiveData<FormValidationStatus>()

    val status: LiveData<FormValidationStatus>
        get() = _status

    private val _currentAlarm = MutableLiveData<Alarm>()

    val currentAlarm: LiveData<Alarm>
        get() = _currentAlarm

    init {
        if (!argument.isNullOrEmpty()) {
            viewModelScope.launch(backgroundDispatcher) {
                val alarm = alarmDao.findById(argument.toInt())
                setTitle(alarm.title.orEmpty())
                setDescription(alarm.description.orEmpty())
                setDate(alarm.date.orEmpty())
                setTime(Time(alarm.hour ?: 0, alarm.min ?: 0))
                setRepeatType(alarm.repeat.orEmpty())
                _currentAlarm.postValue(alarm)
            }
        }

    }


    fun saveAlarm() {
        if (formData.title.isNullOrEmpty()) {
            _status.value = FormValidationStatus.TITLE
        } else if (formData.date.isNullOrEmpty()) {
            _status.value = FormValidationStatus.DATE
        } else if (formData.time == null || (formData.time?.hour == 0 && formData.time?.hour == 0)) {
            _status.value = FormValidationStatus.TIME
        } else if (formData.repeat.isNullOrEmpty()) {
            _status.value = FormValidationStatus.TYPE
        } else {
            saveAlarmToDatabase()
            _status.value = FormValidationStatus.SUCCESS
        }

    }

    private fun saveAlarmToDatabase() {
        viewModelScope.launch(backgroundDispatcher) {
            val alarm = _currentAlarm.value
            if (alarm != null) {
                alarm.title = formData.title
                alarm.date = formData.date
                alarm.description = formData.description
                alarm.repeat = formData.repeat
                alarm.hour = formData.time?.hour
                alarm.min = formData.time?.min
                alarmDao.update(alarm)
            } else {
                alarmDao.insert(
                    Alarm(
                        title = formData.title,
                        description = formData.description,
                        repeat = formData.repeat,
                        date = formData.date,
                        hour = formData.time?.hour,
                        min = formData.time?.min
                    )
                )
            }
        }
    }

    fun setTitle(title: String) {
        formData.title = title
    }

    fun setDescription(description: String) {
        formData.description = description
    }

    fun setDate(date: String) {
        formData.date = date
    }

    fun setTime(time: Time) {
        formData.time = time
    }

    fun setRepeatType(repeatType: String) {
        formData.repeat = repeatType
    }
}

enum class FormValidationStatus(val errorMessage: String) {
    SUCCESS(""),
    TITLE("Please enter title"),
    TIME("Please enter valid time"),
    DATE("Please enter valid date"),
    TYPE("enter proper recurring type");
}