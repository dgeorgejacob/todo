package com.app.todo.feature.alarm.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface AlarmDao {
    @Insert
    fun insert(alarm: Alarm)

    @Update
    fun update(alarm: Alarm)

    @Query("SELECT * FROM Alarm")
    fun getAll(): List<Alarm>

    @Query("SELECT * FROM Alarm WHERE uid=:id")
    fun findById(id: Int):Alarm


}