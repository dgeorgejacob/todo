package com.app.todo.feature.login.repository

import com.app.todo.feature.login.repository.request.LoginRequest
import com.app.todo.network.NetworkConfig

object LoginRepository {

    private val service = NetworkConfig.buildService(LoginService::class.java)

    suspend fun login(loginRequest: LoginRequest) = service.fetchLoginDetails(loginRequest)
}