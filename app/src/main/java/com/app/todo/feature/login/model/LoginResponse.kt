package com.app.todo.feature.login.model

data class LoginResponse(
    val email:String,
    val id:String,
    val createdAt:String
)
