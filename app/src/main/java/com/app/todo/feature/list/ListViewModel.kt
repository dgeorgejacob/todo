package com.app.todo.feature.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.todo.feature.alarm.room.Alarm
import com.app.todo.feature.alarm.room.AlarmDao
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ListViewModel(
    private val alarmDao: AlarmDao,
    private val backgroundDispatcher: CoroutineDispatcher = Dispatchers.Default
) : ViewModel() {
    private val _alarm = MutableLiveData<List<Alarm>>()

    val alarm: LiveData<List<Alarm>>
        get() = _alarm

    init {
        getAlarms()
    }

    private fun getAlarms() {
        viewModelScope.launch(backgroundDispatcher) {
            _alarm.postValue(alarmDao.getAll())
        }
    }
}