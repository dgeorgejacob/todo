package com.app.todo.feature

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.app.todo.feature.alarm.AlarmUI
import com.app.todo.feature.alarm.AlarmViewModel
import com.app.todo.feature.list.AlarmList
import com.app.todo.feature.list.ListViewModel
import com.app.todo.feature.login.LoginScreen
import com.app.todo.feature.login.LoginViewModel
import com.app.todo.ui.theme.ToDoTheme

@Composable
fun TodoNav() {
    ToDoTheme {
        Scaffold {
            NavigationComponent()
        }
    }
}

@Composable
fun NavigationComponent() {
    val navController = rememberNavController()
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        NavHost(
            navController = navController,
            startDestination = NAVIGATION_LOGIN
        ) {
            composable(NAVIGATION_LOGIN) {
                val loginViewModel: LoginViewModel =
                    viewModel(factory = ToDoViewModelFactory(ViewModelType.LOGIN))
                LoginScreen(loginViewModel, navController)

            }

            composable(NAVIGATION_TODO_LIST) {
                val listViewModel: ListViewModel =
                    viewModel(factory = ToDoViewModelFactory(ViewModelType.TODO_LIST))
                AlarmList(listViewModel, navController)
            }

            composable(route = NAVIGATION_FORM, arguments = listOf(navArgument("alarm_id") {
                type = NavType.StringType
            })) { backStackEntry ->
                val argument = backStackEntry.arguments?.getString("alarm_id")
                val formViewModel: AlarmViewModel =
                    viewModel(factory = ToDoViewModelFactory(ViewModelType.DETAILS, argument))
                AlarmUI(formViewModel)
            }
        }

    }

}


@Preview
@Composable
fun ToDoNavPreview() {
    TodoNav()
}