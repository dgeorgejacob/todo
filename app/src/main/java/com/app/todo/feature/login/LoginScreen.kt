package com.app.todo.feature.login

import android.os.Handler
import android.os.Looper
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.app.todo.R
import com.app.todo.feature.NAVIGATION_TODO_LIST
import com.app.todo.feature.login.model.LoginStatus
import com.app.todo.ui.elements.FormTextField

@Composable
fun LoginScreen(loginViewModel: LoginViewModel?, navController: NavHostController) {

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        var email: String? = null
        var password: String? = null
        FormTextField(
            placeHolderText = stringResource(id = R.string.email),
            keyboardType = KeyboardType.Email
        ) {
            email = it
        }
        FormTextField(
            placeHolderText = stringResource(id = R.string.password),
            keyboardType = KeyboardType.Password
        ) {
            password = it
        }

        LoginStatus(loginViewModel)

        LoginButton {
            /**
             * You should only call navigate() as part of a callback and not as part of your
             * composable itself, to avoid calling navigate() on every recomposition.
             *
             *https://developer.android.com/jetpack/compose/navigation
             *
             * FIXME : Find alternate solution
             */
            loginViewModel?.loginWithCredentials(email, password) {
                Handler(Looper.getMainLooper()).post {
                    navController.navigate(NAVIGATION_TODO_LIST)
                }
            }
        }

    }

}



@Composable
fun LoginStatus(loginViewModel: LoginViewModel?) {
    val isErrorInValidation =
        loginViewModel?.loginStatus?.observeAsState()?.value
            ?: LoginStatus.UNKNOWN
    if (isErrorInValidation == LoginStatus.VALIDATION_ERROR || isErrorInValidation == LoginStatus.API_ERROR) {
        val errorMessage = loginViewModel?.getAPIErrorMessage()
        Text(
            text = errorMessage ?: stringResource(id = R.string.error_message_credential),
            color = Color.Red,
            modifier = Modifier.padding(top = 20.dp)
        )
    }
}

@Composable
fun LoginButton(loginClicked: () -> Unit) {
    TextButton(onClick = { loginClicked() }, modifier = Modifier.padding(top = 20.dp)) {
        Text(text = stringResource(id = R.string.login))

    }
}

@Preview(showSystemUi = true)
@Composable
fun PreviewLogin() {
    LoginScreen(loginViewModel = null, navController = rememberNavController())
}