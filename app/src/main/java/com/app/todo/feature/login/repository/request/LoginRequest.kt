package com.app.todo.feature.login.repository.request

data class LoginRequest(
    val email:String,
    val password:String
)
