package com.app.todo.feature.alarm.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Alarm(
    @PrimaryKey(autoGenerate = true) var uid: Int = 0,
    @ColumnInfo(name = "title") var title: String?,
    @ColumnInfo(name = "description") var description: String?,
    @ColumnInfo(name = "repeat") var repeat: String?,
    @ColumnInfo(name = "date") var date: String?,
    @ColumnInfo(name = "hour") var hour: Int?,
    @ColumnInfo(name = "min") var min: Int?,

    )
