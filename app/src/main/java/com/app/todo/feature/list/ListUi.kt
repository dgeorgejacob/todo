package com.app.todo.feature.list

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.app.todo.R

@Composable
fun AlarmList(listViewModel: ListViewModel, navController: NavHostController) {
    val alarms = listViewModel.alarm.observeAsState().value ?: emptyList()
    LazyColumn {
        items(alarms) { alarm ->
            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(15.dp)
                    .clickable { navController.navigate("todo_form/"+alarm.uid.toString()) },
                elevation = 10.dp
            ) {
                Column(Modifier.padding(PaddingValues(15.dp))) {
                    Text(text = alarm.title ?: "")
                    Text(text = stringResource(id = R.string.description) + ": ${alarm.description}")
                    Text(text = stringResource(id = R.string.date) + ": ${alarm.date}")
                    Text(text = stringResource(id = R.string.time) + ": ${alarm.hour} : ${alarm.min}")

                }
            }
        }
    }
}