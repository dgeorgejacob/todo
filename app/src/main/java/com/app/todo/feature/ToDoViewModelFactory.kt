package com.app.todo.feature

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.todo.application.ToDoApplication
import com.app.todo.feature.alarm.AlarmViewModel
import com.app.todo.feature.alarm.room.createDataBase
import com.app.todo.feature.list.ListViewModel
import com.app.todo.feature.login.LoginViewModel
import com.app.todo.feature.login.repository.LoginRepository

class ToDoViewModelFactory(private val type: ViewModelType,private val argument: String? = null) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        if (type == ViewModelType.LOGIN && modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(LoginRepository) as T
        } else if (type == ViewModelType.DETAILS && modelClass.isAssignableFrom(AlarmViewModel::class.java)) {
            return ToDoApplication.getContext()
                ?.let { context -> createDataBase(context = context) }?.let { database ->
                    AlarmViewModel(
                        database.alarmDao(),
                        argument
                    )
                } as T
        } else if (type == ViewModelType.TODO_LIST && modelClass.isAssignableFrom(ListViewModel::class.java)) {
            return ToDoApplication.getContext()
                ?.let { context -> createDataBase(context = context) }?.let { database ->
                    ListViewModel(
                        database.alarmDao()
                    )
                } as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

enum class ViewModelType {
    LOGIN, TODO_LIST, DETAILS
}