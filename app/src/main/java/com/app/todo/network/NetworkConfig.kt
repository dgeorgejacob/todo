package com.app.todo.network

import com.haroldadmin.cnradapter.NetworkResponseAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NetworkConfig {
    val baseUrl = " https://reqres.in/api/"

    private fun getRetroInstance(): Retrofit {

        val client = OkHttpClient.Builder().addInterceptor(LoggingInterceptor()).build()

        return Retrofit.Builder().baseUrl(baseUrl)
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    fun <T> buildService(service: Class<T>): T {
        return getRetroInstance().create(service)
    }

}