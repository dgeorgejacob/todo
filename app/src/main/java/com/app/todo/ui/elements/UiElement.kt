package com.app.todo.ui.elements

import android.widget.CalendarView
import android.widget.TimePicker
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.app.todo.R
import com.app.todo.dateFormat
import java.util.*

@Composable
fun FormTextField(
    placeHolderText: String,
    defaultText: String? = null,
    keyboardType: KeyboardType,
    onValueChanged: (String) -> Unit
) {
    var text by remember { mutableStateOf("") }
    OutlinedTextField(
        value = if (text.isEmpty()) defaultText.orEmpty() else text,
        onValueChange = {
            text = it
            onValueChanged(text)
        },
        label = { Text(placeHolderText) },
        keyboardOptions = KeyboardOptions(keyboardType = keyboardType),
        visualTransformation = if (keyboardType == KeyboardType.Password) {
            PasswordVisualTransformation()
        } else {
            VisualTransformation.None
        }
    )
}

@Composable
fun CalendarInput(
    @StringRes placeHolderText: Int,
    @DrawableRes icon: Int,
    defaultText: String? = null,
    dateResult: (String) -> Unit
) {
    var text by remember { mutableStateOf("") }
    var showCalendar by remember {
        mutableStateOf(false)
    }
    Column {
        OutlinedTextField(
            value = if (text.isEmpty()) defaultText.orEmpty() else text,
            onValueChange = {},
            modifier = Modifier.clickable {
                showCalendar = true
            },
            label = { Text(stringResource(id = placeHolderText)) },
            leadingIcon = { Icon(painter = painterResource(id = icon), contentDescription = "") },
            readOnly = true,
            enabled = false

        )
        TodoCalendar(showCalendar, text) {
            dateResult(it)
            text = it
            showCalendar = false
        }
    }

}

@Composable
fun TodoCalendar(showCalendar: Boolean, selectedDate: String, onConfirmed: (date: String) -> Unit) {
    if (showCalendar) {
        AlertDialog(
            onDismissRequest = {},
            title = {
                Text(stringResource(id = R.string.date))
            },
            confirmButton = {},
            text = {
                AndroidView({
                    val calendar = CalendarView(it)
                    if (selectedDate.isNotEmpty()) {
                        calendar.date = dateFormat.parse(selectedDate).time
                    }
                    calendar
                },
                    Modifier.wrapContentSize(),
                    update = { view ->
                        view.setOnDateChangeListener { _, year, mon, dom ->
                            val calendar = Calendar.getInstance()
                            calendar.set(year, mon, dom, 0, 0)
                            onConfirmed(dateFormat.format(calendar.time))
                        }
                    }
                )
            },
        )

    }
}

@Composable
fun TimeInput(
    @StringRes placeHolderText: Int,
    @DrawableRes icon: Int,
    defaultTime: Time? = null,
    timeResult: (Time) -> Unit
) {
    var timeState by remember { mutableStateOf(defaultTime?:Time(0, 0)) }
    var showTimePicker by remember {
        mutableStateOf(false)
    }
    Column {
        OutlinedTextField(
            value = "${if (timeState.hour == 0) defaultTime?.hour else timeState.hour}:${if (timeState.min == 0) defaultTime?.min else timeState.min}",
            onValueChange = {},
            modifier = Modifier.clickable {
                showTimePicker = true
            },
            label = { Text(stringResource(id = placeHolderText)) },
            leadingIcon = { Icon(painter = painterResource(id = icon), contentDescription = "") },
            readOnly = true,
            enabled = false

        )
        TodoTime(showTimePicker, timeState) { time ->
            if (time != null) {
                timeResult(time)
                timeState = time
            }
            showTimePicker = false
        }
    }

}

@Composable
fun TodoTime(showTimePicker: Boolean, selectedTime: Time, onConfirmed: (Time?) -> Unit) {
    if (showTimePicker) {
        var time: Time? = null
        AlertDialog(
            onDismissRequest = {},
            title = {
                Text(stringResource(id = R.string.time))
            },
            confirmButton = {
                Button(onClick = { onConfirmed(time) }) {
                    Text(text = "Confirm")
                }
            },
            text = {
                AndroidView({
                    val timePicker = TimePicker(it)
                    if (selectedTime.hour == 0 && selectedTime.min == 0) {
                        time = Time(timePicker.hour, timePicker.minute)
                    } else {
                        timePicker.hour = selectedTime.hour
                        timePicker.minute = selectedTime.min
                    }
                    timePicker
                },
                    Modifier.wrapContentSize(),
                    update = { view ->
                        view.setOnTimeChangedListener { _, hour, min ->
                            time = Time(hour = hour, min = min)
                        }
                    }
                )
            },
        )

    }
}

@Composable
fun RepeatTypeRadioGroup(selectedRadioButton: (String) -> Unit) {
    var selected by remember { mutableStateOf("Daily") }
    Row(modifier = Modifier.padding(top = 10.dp)) {
        RadioButton(
            selected = selected == "Daily",
            onClick = {
                selected = "Daily"
                selectedRadioButton(selected)
            })
        Text(
            text = "Daily",
            modifier = Modifier
                .clickable(onClick = { selected = "Daily" })
                .padding(start = 4.dp)
        )
        Spacer(modifier = Modifier.size(4.dp))
        RadioButton(selected = selected == "Weekly", onClick = {
            selected = "Weekly"
            selectedRadioButton(selected)
        })
        Text(
            text = "Weekly",
            modifier = Modifier
                .clickable(onClick = {
                    selected = "Weekly"
                    selectedRadioButton(selected)
                })
                .padding(start = 4.dp)
        )
    }
}


@Preview
@Composable
fun CalendarInputPreview() {
    CalendarInput(placeHolderText = R.string.date, icon = R.drawable.ic_calendar) {}
}

@Preview
@Composable
fun TimeInputPreview() {
    TimeInput(placeHolderText = R.string.time, icon = R.drawable.ic_time_24) {}
}

data class Time(val hour: Int, val min: Int)