package com.app.todo.application

import android.app.Application
import android.content.Context

class ToDoApplication : Application() {

    companion object {
        private var sApplication: Application? = null

        fun getContext(): Context? {
            return sApplication?.applicationContext
        }
    }


    override fun onCreate() {
        super.onCreate()
        sApplication = this;
    }

}