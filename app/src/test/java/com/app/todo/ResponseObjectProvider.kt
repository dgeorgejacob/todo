package com.app.todo

import com.google.gson.Gson
import com.haroldadmin.cnradapter.NetworkResponse
import java.io.File

internal object ResponseObjectProvider {
    private const val PATH_URL = "src/test/resources/mocks/"


    fun <T : Any> getMockResponse(fileName: String, response: Class<T>): NetworkResponse.Success<T> {
        val file = File("$PATH_URL${fileName}")
        Gson().fromJson(file.readText(), response)

        return NetworkResponse.Success(Gson().fromJson(file.readText(), response),null,200)
    }

}