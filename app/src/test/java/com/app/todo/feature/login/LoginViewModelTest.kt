package com.app.todo.feature.login

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.app.todo.CoroutineTestRule
import com.app.todo.ResponseObjectProvider
import com.app.todo.feature.login.model.LoginResponse
import com.app.todo.feature.login.repository.LoginRepository
import com.app.todo.feature.login.repository.request.LoginRequest
import io.mockk.coEvery
import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import com.app.todo.feature.login.model.LoginStatus

class LoginViewModelTest {

    @Rule
    @JvmField
    val rule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutineTestRule()

    private val repository = mockk<LoginRepository>()

    @Test
    fun `verify valid input data`() {
        val loginViewModel =
            LoginViewModel(repository = repository, coroutinesTestRule.testDispatcher)
        loginViewModel.loginWithCredentials("abc", "df") {}
        val result = loginViewModel.loginStatus.value
        assertNotNull(result)
        assertTrue("Validation should fail", result == LoginStatus.VALIDATION_ERROR)

    }

    @Test
    fun `verify login`() {
        val loginViewModel =
            LoginViewModel(repository = repository, coroutinesTestRule.testDispatcher)

        coEvery {
            repository.login(LoginRequest("eve.holt@reqres.in", "cityslicka"))
        } returns
                ResponseObjectProvider.getMockResponse(
                    "success.json",
                    LoginResponse::class.java
                )
        loginViewModel.loginWithCredentials("eve.holt@reqres.in", "cityslicka") {}
        val result = loginViewModel.loginStatus.value
        assertNotNull(result)
        assertTrue("Validation should pass", result == LoginStatus.COMPLETED)

    }

}